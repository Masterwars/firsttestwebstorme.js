const expect = require('chai').expect;
const avg = require('../avg')


it('should calculate average for array of  positivs', function () {
    expect(avg([2, 3, 4, 5, 3])).eq(3.4)

});
it('should calculate average for array of  neg', function () {
    expect(avg([-2, -3, -4, -5, -3])).eq(-3.4)

});
it('should return 0 for empty array', function () {
    expect(avg([])).eq(0)
});
it('should return 0 without arg', function () {
    expect(avg()).eq(0)
});
it('should return 0 if arg NULL', function () {
    expect(avg(null)).eq(0)
});

