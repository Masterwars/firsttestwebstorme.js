const expect = require('chai').expect;
const sum = require('../sum')



it('calculate', function () {
    expect(sum(2, 3)).eq(5)

});
it('calculate negative numbers', function () {
    expect(sum(-2, -4)).eq(-6)

});
it('should return 0 if arg NULL', function () {
    expect(sum(null)).eq(0)
});