const expect = require('chai').expect;
const compare = require('../compare')



it('compare numbers', function () {
    expect(compare([2], [3])).eq('b')

});

it('compare null and 0', function () {
    expect(compare([null], [0])).eq('b')

});
it('compare letters', function () {
    expect(compare(['f'], ['c'])).eq('a')

});

it('compare words', function () {
    expect(compare(['PASV'], ['CLIENT'])).eq('a')

});

it('compare signs', function (param) {
    expect(compare(['!'], ['?'])).eq('b')

});