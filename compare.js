function compare(a, b) {
    if (a < b) {
        return 'b';
    }
    if (a > b) {
        return 'a';
    }

    return 0;
};

module.exports = compare;